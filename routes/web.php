<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('homepage');

Route::get('/aboutus', 'PageController@aboutus')->name('aboutus');
Route::get('/who-we-are', 'PageController@whoweare')->name('whoweare');
Route::get('/completed-projects', 'PageController@portfolio')->name('portfolio');
Route::get('/what-we-do', 'PageController@services')->name('whatwedo');
Route::get('/our-competencies', 'PageController@ourcompetencies')->name('ourcompetencies');
Route::get('/cost-summary', 'PageController@costsummary')->name('costsummary');
Route::get('/process-flow', 'PageController@processflow')->name('processflow');
Route::get('/bebefits-we-offer', 'PageController@benefitsweoffer')->name('benefitsweoffer');
Route::get('/our-team', 'PageController@team')->name('team');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/new-project', 'PageController@newproject')->name('new-project');
Route::post('/send-mail', 'PageController@sendEmail')->name('send-mail');

Route::get('/store', 'storeController@index')->name('store');

//Account 
Route::get('/register', 'FrontAuthController@registerPage')->name('register');
Route::get('/login', 'FrontAuthController@loginPage')->name('login');
Route::get('/logout', 'FrontAuthController@logout')->name('account-logout');
Route::post('/sign-up', 'FrontAuthController@signup')->name('sign-up');
Route::post('/login-validate', 'FrontAuthController@login')->name('login-validate');
Route::get('/dashboard', 'AccountController@dashboard')->name('account-dashboard');

//PORTFOLIO PAGES
Route::get('/project/offsure', 'FrontPortfolioController@project1')->name('project1');
Route::get('/project/cocoon-house', 'FrontPortfolioController@project2')->name('project2');

//BLOG PLAGE
Route::get('/blog', 'PageController@blog')->name('blog');
Route::get('/blog/two-tips-to-sustain-your-rtos-financial-life', 'PageController@blog1')->name('blog1');
Route::get('/blog/5-rto-functions-that-can-be-completed-offshore', 'PageController@blog2')->name('blog2');


//ADMIN ROUTINGS
Route::get('/admin', 'AdminController@login')->name('admin-login');
Route::post('/auth/checklogin','AdminController@checklogin')->name('checklogin');
Route::get('/auth/logout','AdminController@logout')->name('logout');
Route::get('/admin/dashboard', 'DashboardController@index')->name('dashboard');

//Projects
Route::get('/admin/projects','ProjectsController@index')->name('projects');
Route::get('/admin/projects-add','ProjectsController@add')->name('projects-add');
Route::post('/admin/projects-add-action','ProjectsController@addAction')->name('projects-add-action');
Route::get('/admin/projects-category','ProjectsController@category')->name('projects-category');
Route::post('/admin/projects-add-category','ProjectsController@addCategory')->name('projects-add-category');

//Blogs
Route::get('/admin/blog-category','BlogsController@category')->name('blog-category');
Route::post('/admin/add-blog-category','BlogsController@addCategory')->name('add-blog-category');

Route::post('/payment', 'storeController@payWithpaypal')->name('payment');
Route::get('/status', 'storeController@getPaymentStatus')->name('status');


