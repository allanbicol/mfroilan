<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-category-one', 'CategoryController@getCategoryOne');
Route::get('get-category-two/{parent_id}', 'CategoryController@getCategoryTwo');
Route::get('get-category-three/{parent_id}', 'CategoryController@getCategoryThree');

Route::post('post-search-pool', 'CategoryController@savePoolSearch');
Route::get('get-search-pool', 'CategoryController@getPoolSearch');
