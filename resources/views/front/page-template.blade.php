<!DOCTYPE html>
<html lang="en">

<head>
   <!--
    Basic Page Needs
    ==================================================
    -->
   <meta charset="utf-8">
   @yield('meta-title')
   <!--
    Mobile Specific Metas
    ==================================================
    -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <meta name="google-site-verification" content="TogTt44wbL5Yg_Zfg1bv1RweoCxw9TFKnji-i29OnXw" />
   @yield('meta')
   <!--
    CSS
    ==================================================
    -->
   <!-- Bootstrap-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/bootstrap.min.css')}}">
   <!-- Animation-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/animate.css')}}">
   <!-- Morris CSS -->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/morris.css')}}">
   <!-- FontAwesome-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/font-awesome.min.css')}}">
   <!-- Icon font-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/icon-font.css')}}">
   <!-- Owl Carousel-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/owl.carousel.min.css')}}">
   <!-- Owl Theme -->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/owl.theme.default.min.css')}}">
   <!-- Colorbox-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/colorbox.css')}}">
   <!-- Template styles-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/style.css')}}">
   <!-- Responsive styles-->
   <link rel="stylesheet" href="{{ URL::asset('front-theme/css/responsive.css')}}">
   <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file.-->
   <!--if lt IE 9
    script(src='js/html5shiv.js')
    script(src='js/respond.min.js')
    -->
</head>
@yield('structure')

<body>

   <div class="body-inner">

      <div class="site-top">
         <div class="topbar-transparent" id="top-bar">
            <div class="container">
               <div class="row">
                  <div class="col-lg-9 col-md-9 col-sm-12">
                     <ul class="top-info">
                        <li><span class="info-icon"><i class="icon icon-phone3"></i></span>
                           <div class="info-wrapper">
                              <p class="info-title">+639087491763</p>
                           </div>
                        </li>
                        <li><span class="info-icon"><i class="icon icon-envelope"></i></span>
                           <div class="info-wrapper">
                              <p class="info-title">admin@mfroilan.com</p>
                           </div>
                        </li>
                        <li class="last"><span class="info-icon"><i class="icon icon-map-marker2"></i></span>
                           <div class="info-wrapper">
                              <p class="info-title">Northern Samar, Philippines</p>
                           </div>
                        </li>
                     </ul>
                     <!-- Ul end-->
                  </div>
                  <!--Top info end-->
                  <div class="col-lg-3 col-md-3 col-sm-12 text-lg-right text-md-center">
                     <ul class="top-social">
                        <li>
                          @if(Auth::user())
                           <li>
                           <a title="login" class="header_link" href="{{route('account-dashboard')}}">
                               My Account
                           </a>
                           <a title="register" class="header_link" href="{{route('account-logout')}}">
                                 Logout
                           </a>
                        @else
                        <li>
                           <a title="login" class="header_link" href="{{route('login')}}">
                               Login
                        </a>
                        <a title="register" class="header_link" href="{{route('register')}}">
                                Sign up
                         </a>
                         @endif
                           {{-- <a title="Facebook" href="#">
                            <span class="social-icon"><i class="fa fa-facebook"></i></span>
                         </a>
                           <a title="Twitter" href="#">
                            <span class="social-icon"><i class="fa fa-twitter"></i></span>
                         </a>
                           <a title="Google+" href="#">
                            <span class="social-icon"><i class="fa fa-google-plus"></i></span>
                         </a>
                           <a title="Linkdin" href="#">
                            <span class="social-icon"><i class="fa fa-linkedin"></i></span>
                         </a>
                           <a title="Skype" href="#">
                            <span class="social-icon"><i class="fa fa-instagram"></i></span>
                         </a> --}}
                        </li>
                        <!-- List End -->
                     </ul>
                     <!-- Top Social End -->
                  </div>
                  <!--Col end-->
               </div>
               <!--Content row end-->
            </div>
            <!--Container end-->
         </div>
         <!--Top bar end-->
         <header class="header-trans-leftbox" id="header">
                <div class="container">
                   <div class="header-wrapper clearfix">
                      <div class="site-nav-inner">
                         <nav class="navbar navbar-expand-lg">
                            <div class="navbar-brand navbar-header">
                               <div class="logo">
                                <a href="{{route('homepage')}}">
                                     <img src="{{ URL::asset('front-theme/images/logo2.png')}}" width="100px" alt="">
                                  </a>
                               </div>
                               <!-- logo end-->
                            </div>
                            <!-- Navbar brand end-->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                               aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span></button>
                            <!-- End of Navbar toggler-->
                            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                               <ul class="navbar-nav">
                                  <li class="nav-item dropdown active">
                                     <a class="nav-link" href="{{route('homepage')}}">Home</a>
                                     <!-- <ul class="dropdown-menu" role="menu">
                                        <li class="active"><a href="index.html">Home One</a></li>
                                        <li><a href="index-2.html">Home Two</a></li>
                                        <li><a href="index-3.html">Home Three</a></li>
                                        <li><a href="index-4.html">Home Four</a></li>
                                        <li><a href="index-5.html">Home Five</a></li>
                                        <li><a href="index-6.html">Home Six</a></li>
                                        <li><a href="index-7.html">Home Seven</a></li>
                                        <li><a href="index-8.html">Home Eight</a></li>
                                        <li><a href="index-9.html">Home Nine</a></li>
                                     </ul> -->
                                  </li>
                                  <!-- Home li end-->
                                  {{-- <li class="nav-item dropdown"><a class="nav-link" href="#" data-toggle="dropdown">Executive Summary<i class="fa fa-angle-down"></i></a>
                                     <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{route('whoweare')}}">Who We Are</a></li>
                                        <li><a href="{{route('whatwedo')}}">What We Do</a></li>
                                        <li><a href="{{route('ourcompetencies')}}">Our Competencies</a></li>

                                     </ul>
                                  </li> --}}
                                  <li class="nav-item dropdown"><a class="nav-link" href="{{route('whatwedo')}}" >What We Do</a></li>
                                  <!-- Page li end-->
                                  {{-- <li class="nav-item dropdown"><a class="nav-link" href="{{route('costsummary')}}" >Cost Summary</a></li> --}}
                                  <li class="nav-item dropdown"><a class="nav-link" href="{{route('store')}}" >Store</a></li>
                                  <li class="nav-item dropdown"><a class="nav-link" href="{{route('blog')}}" >Blog</a></li>
                    
                                  <!-- Page li end-->
                                  {{-- <li class="nav-item dropdown"><a class="nav-link" href="{{route('benefitsweoffer')}}">Benefits we Offer</a>
                                  </li>
                                  <li class="nav-item dropdown"><a class="nav-link" href="{{route('portfolio')}}">Projects</a>
                                  </li>
                                  <li class="nav-item dropdown"><a class="nav-link" href="{{route('team')}}">Our Team</a>
                                  </li> --}}
                                  <!-- Features li end
                                  <li class="nav-item dropdown">
                                     <a class="nav-link" href="#" data-toggle="dropdown">
                                  Blog
                               </a>
                                     <ul class="dropdown-menu" role="menu">
                                        <li><a href="news-left-sidebar.html">News Left Sidebar</a></li>
                                        <li><a href="news-right-sidebar.html">News Right Sidebar</a></li>
                                        <li><a href="news-single.html">News Single</a></li>
                                     </ul>
                                  </li>-->
                                  <!-- News li end-->
                                  <li class="nav-item dropdown">
                                     <a class="nav-link" href="{{route('contact')}}" >Contact</a>

                                  </li>
                                  <!-- Contact li End -->
                               </ul>
                               <!--Nav ul end-->
                            </div>
                         </nav>

                      </div>
                      <!--Site nav inner end-->
                   </div>
                   <!-- Header wrapper end-->
                </div>
                <!--Container end-->
             </header>
             <!--Header end-->
      </div>

      <div class="banner-area" id="banner-area" style="background-image:url({{ URL::asset('front-theme/images/banner/banner2.jpg')}});">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  @yield('title')
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->


         @yield('content')


      <div class="gap-60"></div>

      <!-- Footer start-->
      <footer class="footer" id="footer">
         <div class="footer-top">
            <div class="container">
               <div class="footer-top-bg row">
                  <div class="col-lg-4 footer-box"><i class="icon icon-map-marker2"></i>
                     <div class="footer-box-content">
                        <h3>Head Office</h3>
                        <p>Northern Samar, Philippines</p>
                     </div>
                  </div>
                  <!-- Box 1 end-->
                  <div class="col-lg-4 footer-box"><i class="icon icon-phone3"></i>
                     <div class="footer-box-content">
                        <h3>Call Us</h3>
                        <p>+639087491763</p>
                     </div>
                  </div>
                  <!-- Box 2 end-->
                  <div class="col-lg-4 footer-box"><i class="icon icon-envelope"></i>
                     <div class="footer-box-content">
                        <h3>Mail Us</h3>
                        <p>admin@mfroilan.com</p>
                     </div>
                  </div>
                  <!-- Box 3 end-->
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Footer top end-->
         <div class="footer-main bg-overlay">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4 col-md-12 footer-widget footer-about">
                     <div class="footer-logo">
                        <a href="index.html">
                           <img src="{{ URL::asset('front-theme/images/logo2.png')}}" width="100px" alt="">
                        </a>
                     </div>
                     <p>
                            We believe in quality and standard worldwide.
                    </p>
                     <div class="footer-social">
                        <ul>
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                           <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                           <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                           <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                     </div>
                     <!-- Footer social end-->
                  </div>
                  <!-- About us end-->
                  <div class="col-lg-4 col-md-12 footer-widget">
                     <h3 class="widget-title">Useful Links</h3>
                     <ul class="list-dash">
                        {{-- <li><a href="{{route('whoweare')}}">Who We Are</a></li> --}}
                        <li><a href="{{route('whatwedo')}}">What We Do</a></li>
                        {{-- <li><a href="{{route('ourcompetencies')}}">Our Competences</a></li> --}}
                        {{-- <li><a href="{{route('costsummary')}}">Cost Summary</a></li> --}}
                        {{-- <li><a href="{{route('processflow')}}">Process Flow</a></li> --}}
                        {{-- <li><a href="{{route('benefitsweoffer')}}">Benefits We Offer</a></li> --}}
                        {{-- <li><a href="{{route('portfolio')}}">Projects</a></li> --}}
                        <li><a href="{{route('store')}}">Store</a></li>
                        <li><a href="{{route('blog')}}">Blog</a></li>
                        <li><a href="{{route('contact')}}">Contact Us</a></li>
                     </ul>
                  </div>
                  <div class="col-lg-4 col-md-12W footer-widget">
                     <h3 class="widget-title">Subscribe</h3>
                     <div class="newsletter-introtext">Don’t miss to subscribe to our new feeds, kindly fill the form below.</div>
                     <form class="newsletter-form" id="newsletter-form" action="#" method="post">
                        <div class="form-group">
                           <input class="form-control form-control-lg" id="newsletter-form-email" type="email" name="email" placeholder="Email Address"
                              autocomplete="off">
                           <button class="btn btn-primary"><i class="fa fa-paper-plane"></i></button>
                        </div>
                     </form>
                  </div>
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Footer Main-->
         <div class="copyright">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-12">
                     <div class="copyright-info"><span>Copyright © 2018 MFroilan. All Rights Reserved.</span></div>
                  </div>
                  <div class="col-lg-6 col-md-12">
                     <div class="footer-menu">
                        <ul class="nav unstyled">
                           <li><a href="#">About</a></li>
                           <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">Investors</a></li>
                           <li><a href="#">Legals</a></li>
                           <li><a href="#">Contact</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <!-- Row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Copyright end-->
      </footer>
      <!-- Footer end-->

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/jquery.js')}}"></script>
      <!-- Popper-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/popper.min.js')}}"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/bootstrap.min.js')}}"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/owl.carousel.min.js')}}"></script>
      <!-- Counter-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/jquery.counterup.min.js')}}"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/waypoints.min.js')}}"></script>
      <!-- Color box-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/jquery.colorbox.js')}}"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/smoothscroll.js')}}"></script>
      <!-- Google Map API Key-->
      <!-- <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script> -->
      <!-- Google Map Plugin-->
      <!-- <script type="text/javascript" src="js/gmap3.js"></script> -->
      <!-- Template custom-->
      <script type="text/javascript" src="{{ URL::asset('front-theme/js/custom.js')}}"></script>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144938493-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-144938493-1');
        </script>


   </div>
   <!--Body Inner end-->
</body>

</html>
