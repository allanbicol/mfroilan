@extends('front.page-template')

@section('meta-title')
<title>Two Tips to Sustain Your RTO’s Financial Life</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="date" content="2019-11-12" scheme="YYYY-MM-DD">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="I’ve been working in the VET sector for more than 8 years now. I’ve read and heard RTOs shutting down operations due to unmet government and private obligations after 10 or 5 years of operations. This is too sad to say the least as gigantic efforts have been put in place to kickstart RTO operations. Thus, here are two tips you can observe to sustain your RTO’s financial life and ensure that annual turnover is way higher than your budget.">
<link rel="canonical" href="{{route('blog1')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Two Tips to Sustain Your RTO’s Financial Life">
<meta property="og:description" content="I’ve been working in the VET sector for more than 8 years now. I’ve read and heard RTOs shutting down operations due to unmet government and private obligations after 10 or 5 years of operations. This is too sad to say the least as gigantic efforts have been put in place to kickstart RTO operations. Thus, here are two tips you can observe to sustain your RTO’s financial life and ensure that annual turnover is way higher than your budget.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/blog/blog1.jpg')}}">
<meta property="og:url" content="{{route('blog1')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Two Tips to Sustain Your RTO’s Financial Life">
<meta name="twitter:description" content="I’ve been working in the VET sector for more than 8 years now. I’ve read and heard RTOs shutting down operations due to unmet government and private obligations after 10 or 5 years of operations. This is too sad to say the least as gigantic efforts have been put in place to kickstart RTO operations. Thus, here are two tips you can observe to sustain your RTO’s financial life and ensure that annual turnover is way higher than your budget.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/blog/blog1.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Blog</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Two Tips to Sustain Your RTO’s Financial Life</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container" id="main-container">
<div class="container">
        <div class="row">
                <div class="col-lg-8">
                   <div class="post-content post-single">
                      <div class="post-media post-image">
                         <img class="img-fluid" width="100%" src="{{ URL::asset('front-theme/images/blog/blog1.jpg')}}" alt=""><span class="post-meta-date"><span class="day">12</span>Nov</span>
                      </div>
                      <div class="post-body clearfix">
                         <div class="entry-header">
                            <div class="post-meta"><span class="post-author"><img class="avatar" alt="" src="{{ URL::asset('front-theme/images/team/team1.jpg')}}"><a href="#"> By Mith</a></span>
                               <span class="post-cat"><i class="icon icon-folder"></i><a href="#"> RTO</a></span>
                               <span class="post-comment"><i class="icon icon-comment"></i><a class="comments-link" href="#"></a>0</span>
                               {{-- <span class="post-tag"><i class="icon icon-tag"></i><a href="#"> Insurance, Business</a></span> --}}
                            </div>
                            <h2 class="entry-title"><a href="#">Two Tips to Sustain Your RTO’s Financial Life</a></h2>
                         </div>
                         <!-- header end-->
                         <div class="entry-content">
                            <p>I’ve been working in the VET sector for more than 8 years now. I’ve read and heard RTOs shutting down operations due to unmet government and private obligations after 10 or 5 years of operations. This is too sad to say the least as gigantic efforts have been put in place to kickstart RTO operations. Thus, here are two tips you can observe to sustain your RTO’s financial life and ensure that annual turnover is way higher than your budget. </p>
                            <p>Many families look to the college years for children/grandchildren with mixed emotions excitement
                               and trepidation. These two words rarely go together, but when it comes to college, it seems
                               to be a perfect match.</p>

                            <h3>1. Be efficient in all areas </h3>
                            <p>Ensure that all resources are optimised, be it employees, equipment or office supplies. Monitor employee performance. Do you have employees who ensure your monthly income is enough for everyone or you have everyone making your monthly income not enough?</p>
                            <p>Do you still have hard copies of learner guides and assessment materials that you get to spend every time you need them or a Learning Management System that students and employees can access anytime, anywhere. I know of an RTO that saved $$$ every month by going paperless in its operations. </p>
                            <p>Do you rent an entire building to house less than 10 students every time you hold a training or you have a Learning Management System that cuts your rent expense by at least 50%?</p>
                            <p>What I am trying to say here is, look at your RTO’s efficiency in terms of cash and human resources. Review your procedures and processes and dramatically cut your monthly expenses. </p>

                            <h3>2. Delegate back office tasks offshore </h3>
                            <p>Do you pay $45, 000 monthly for answering phone and email enquiries? Or pay $90,000 or so to assist your managers in maintaining compliance and making sure there enough enrolled students every month? Imagine having to raise these amounts every month? You need at least 30 students enrolled in a $4,500 course to breakeven. </p>
                            <p>Have you tried offshoring administrative support and compliance support to other countries where these tasks can be efficiently handled by qualified professionals and giving you 50% cost savings?</p>
                            <p>With the presence of technology, Learning Management Systems and office documents that require updating or sharing can be accessed anywhere in the world. You can email anytime, send instructions over Whatsapp, Skype, or Viber. </p>
                            <p>Task completion wise there is Trello, Asana, Basecamp, and other project management or task monitoring platform.</p>
                            <p>It is very convenient to delegate tasks offshore 24/7. You name it, you have it. </p>
                            <p>Think about these tips you can implement to sustain your RTO’s financial life 20, 30 or 50 years down the road. Get hold of your annual financial budget, cross down unnecessary cost burdens and sustain your RTO’s financial life for a lifetime. </p>
                        </div>
                         <!-- entry content end-->
                         {{-- <div class="post-footer clearfix">
                            <div class="post-tags float-left"><strong>Tags: </strong><a href="#">Financial</a><a href="#">Our Advisor</a><a href="#">Market</a></div>

                            <div class="share-items float-right">
                               <ul class="post-social-icons unstyled">
                                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                  <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                               </ul>
                            </div>

                         </div> --}}
                         <!-- Post footer end-->
                      </div>
                      <!-- post-body end-->
                   </div>
                   <!-- Post content end-->
                   <nav class="post-navigation clearfix mrtb-40">
                      <div class="post-previous">
                         <a href="#">&nbsp;
                            {{-- <h3>Bitcoin is the gag gift you should buy this holiday season</h3><span><i class="fa fa-long-arrow-left"></i>Previous Post</span> --}}
                        </a>
                      </div>
                      <div class="post-next">
                         <a href="{{route('blog2')}}">
                            <h3>5 RTO Functions That Can Be Completed Offshore</h3><span>Next Post <i class="fa fa-long-arrow-right"></i></span></a>
                      </div>
                   </nav>
                   {{-- <div class="author-box solid-bg mrb-40">
                      <div class="author-img float-left">
                         <img src="images/news/avator1.png" alt="">
                      </div>
                      <div class="author-info">
                         <div class="post-social-icons float-right unstyled"><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                         <h3>Jonhy Rick</h3>
                         <p class="author-url"><a href="#">www.finances.com</a></p>
                         <p>Life is good. Here we are in the last couple months of summer with its warm weather, family activities,
                            getting ready for the children.</p>
                      </div>
                   </div> --}}
                   <!-- Author box end-->
                   <!-- Post comment start-->
                   {{-- <div class="comments-area" id="comments">
                      <h3 class="comments-heading">07 Comments</h3>
                      <ul class="comments-list">
                         <li>
                            <div class="comment">
                               <img class="comment-avatar float-left" alt="" src="images/news/avator1.png">
                               <div class="comment-body">
                                  <div class="meta-data"><span class="float-right"><a class="comment-reply" href="#"><i class="fa fa-mail-reply-all"></i> Reply</a></span>
                                     <span class="comment-author">Michelle Aimber</span><span class="comment-date">January 04, 2018</span></div>
                                  <div class="comment-content">
                                     <p>Life is good. Here we are in the last couple months of summer with its warm weather,
                                        family activities, getting ready for the children.</p>
                                  </div>
                               </div>
                            </div>

                            <ul class="comments-reply">
                               <li>
                                  <div class="comment">
                                     <img class="comment-avatar float-left" alt="" src="images/news/avator3.png">
                                     <div class="comment-body">
                                        <div class="meta-data"><span class="float-right"><a class="comment-reply" href="#"><i class="fa fa-mail-reply-all"></i> Reply</a></span>
                                           <span class="comment-author">Adam Smith</span><span class="comment-date">January 17, 2018</span></div>
                                        <div class="comment-content">
                                           <p>Life is good. Here we are in the last couple months of summer with its warm weather,
                                              family activities, getting ready for the children.</p>
                                        </div>
                                     </div>
                                  </div>

                               </li>
                            </ul>

                         </li>

                      </ul>

                   </div> --}}
                   <!-- Post comment end-->
                   <div class="comments-form border-box">
                      <h3 class="title-normal">Leave a comment</h3>
                      <form role="form">
                         <div class="row">
                            <div class="col-lg-6">
                               <div class="form-group">
                                  <input class="form-control" id="name" name="name" placeholder="Full Name" type="text" required="">
                               </div>
                            </div>
                            <!-- Col 4 end-->
                            <div class="col-lg-6">
                               <div class="form-group">
                                  <input class="form-control" id="email" name="email" placeholder="Email Address" type="email" required="">
                               </div>
                            </div>
                            <div class="col-lg-12">
                               <div class="form-group">
                                  <input class="form-control" placeholder="Website" type="text" required="">
                               </div>
                            </div>
                            <div class="col-lg-12">
                               <div class="form-group">
                                  <textarea class="form-control required-field" id="message" placeholder="Comments" rows="10" required=""></textarea>
                               </div>
                            </div>
                            <!-- Col 12 end-->
                         </div>
                         <!-- Form row end-->
                         <div class="clearfix text-right">
                            <button class="btn btn-primary" type="submit">Post Comment</button>
                         </div>
                      </form>
                      <!-- Form end-->
                   </div>
                   <!-- Comments form end-->
                </div>
                <!-- Content Col end-->
                <div class="col-lg-4">
                   <div class="sidebar sidebar-right">
                      <div class="widget widget-search">
                         <div class="input-group" id="search">
                            <input class="form-control" placeholder="Search" type="search"><span class="input-group-btn"><i class="fa fa-search"></i></span>
                         </div>
                      </div>
                      <div class="widget recent-posts">
                            <h3 class="widget-title">Popular Posts</h3>
                            <ul class="unstyled clearfix">
                               <li class="media">
                                   <div class="media-left media-middle">
                                       <img alt="img" src="{{ URL::asset('front-theme/images/blog/blog2.jpg')}}">
                                   </div>
                                   <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 13 Nov, 2019</a></span>
                                       <h4 class="entry-title"><a href="{{route('blog2')}}">5 RTO Functions That Can Be Completed Offshore</a>
                                           <small>by Mith</small>
                                       </h4>
                                   </div>
                                   <div class="clearfix"></div>
                                   </li>
                               <!-- 1st post end-->
                               <li class="media">
                                   <div class="media-left media-middle">
                                       <img alt="img" src="{{ URL::asset('front-theme/images/blog/blog1.jpg')}}">
                                   </div>
                                   <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 12 Nov, 2019</a></span>
                                       <h4 class="entry-title"><a href="{{route('blog1')}}">Two Tips to Sustain Your RTO’s Financial Life</a>
                                           <small>by Mith</small>
                                       </h4>
                                   </div>
                                   <div class="clearfix"></div>
                                   </li>
                               <!-- 2nd post end-->

                            </ul>
                         </div>
                      <!-- Recent post end-->
                      {{-- <div class="widget">
                         <h3 class="widget-title">Categories</h3>
                         <ul class="widget-nav-tabs">
                            <li><a href="#">Tax Planning</a><span class="posts-count">(05)</span></li>
                            <li><a href="#">Saving Strategy</a><span class="posts-count">(06)</span></li>
                            <li><a href="#">Financial Planning</a><span class="posts-count">(11)</span></li>
                            <li><a href="#">Mutual Funds</a><span class="posts-count">(09)</span></li>
                            <li><a href="#">Business Loan</a><span class="posts-count">(13)</span></li>
                            <li><a href="#">Insurance Consulting</a><span class="posts-count">(13)</span></li>
                         </ul>
                      </div> --}}
                      <!-- Categories end-->
                      {{-- <div class="widget">
                         <h3 class="widget-title">Archives </h3>
                         <ul class="widget-nav-tabs">
                            <li><a href="#">Feburay 2016</a></li>
                            <li><a href="#">January 2016</a></li>
                            <li><a href="#">December 2015</a></li>
                            <li><a href="#">November 2015</a></li>
                            <li><a href="#">October 2015</a></li>
                         </ul>
                      </div> --}}
                      <!-- Archives end-->
                      <div class="widget widget-tags">
                         <h3 class="widget-title">Tags </h3>
                         <ul class="unstyled clearfix">
                            <li><a href="#">Financial</a></li>
                            <li><a href="#">Tax Planning</a></li>
                            <li><a href="#">Loan</a></li>
                            <li><a href="#">Funds</a></li>
                            <li><a href="#">Finance</a></li>
                            <li><a href="#">Insurance</a></li>
                            <li><a href="#">Consulting</a></li>
                         </ul>
                      </div>
                      <!-- Tags end-->
                   </div>
                   <!-- Sidebar end-->
                </div>
                <!-- Sidebar Col end-->
             </div>
             <!-- Main row end-->
 </div>

 <div class="gap-80"></div>

</section>


@endsection
