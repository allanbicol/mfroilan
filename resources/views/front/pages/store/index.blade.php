@extends('front.page-template')

@section('meta-title')
<title>Cost Summary - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<link rel="canonical" href="{{route('costsummary')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta property="og:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta property="og:url" content="{{route('costsummary')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta name="twitter:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Store</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Store</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services ts-service-pattern" id="ts-services">
        <div class="container">
            {{-- <div class="row text-center">
                <div class="col-md-12">
                <h2 class="section-title">Cost Summary</h2>

                </div>
            </div> --}}
            <!-- Title row end-->
            @if ($message = Session::get('success'))
                    <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>{!! $message !!}</div>
                    <?php Session::forget('success');?>
                    @endif
                
                    @if ($message = Session::get('error'))
                    <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>{!! $message !!}</div>
                    <?php Session::forget('error');?>
                    @endif

            {{-- <div class="row">
                <div class="col-lg-12 col-md-12 text-center">
                    <img src="{{ URL::asset('front-theme/images/maintainance-gif.gif')}}">
                    <br>
                    <br>
                    <h2>This page is under development, training materials will be available shortly.</h2>
                </div>

            </div> --}}
            {{-- <form class="w3-container w3-display-middle w3-card-4 " method="POST" id="payment-form"  action="{{route('payment')}}">
  {{ csrf_field() }}
  <h2 class="w3-text-blue">Product Name</h2>
  <p>Demo PayPal form - Integrating paypal in laravel</p>
  <p>  
  <h4>5.00 USD</h4>    
  <input class="w3-input w3-border" name="amount" type="hidden" value="5"></p>      
  <button class="w3-btn w3-blue">Pay with PayPal</button></p>
</form> --}}
            <div class="row">
        <div class="col-md-4 col-xs-6">
            <div class="card">
                <img class="card-img-top img-fluid" src="//placehold.it/500x200" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="card">
            <img class="card-img-top img-fluid" src="//placehold.it/500x200" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="card">
                <img class="card-img-top img-fluid" src="//placehold.it/500x180" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content. Nullam sapien massa, aliquam in cursus ut, ullamcorper in tortor. 
                     Aliquam codeply mauris arcu, tristique a lobortis vitae, condimentum feugiat justo.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="card">
            <img class="card-img-top img-fluid" src="//placehold.it/500x200" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="card">
            <img class="card-img-top img-fluid" src="//placehold.it/500x200" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">Card title another</h4>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="card">
                <img class="card-img-top img-fluid" src="//placehold.it/500x220" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">This card has even longer content than the first to show that equal height action.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
        

        </div>
        <!-- Container end-->
    </div>
    </section>
@endsection
