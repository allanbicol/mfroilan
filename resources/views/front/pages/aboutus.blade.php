@extends('front.page-template')

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective.">
<link rel="canonical" href="{{route('homepage')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Home - MFroilan Training and Consultancy">
<meta property="og:description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/homepage.jpg')}}">
<meta property="og:url" content="{{route('homepage')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Home - MFroilan Training and Consultancy">
<meta name="twitter:description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/homepage.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection


@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">About Us</h1>
        <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li>About Us</li>
        </ol>
    </div>
@endsection

@section('content')
<div class="container">
    <div class="row">
       <div class="col-lg-6">
          <h2 class="column-title title-small"><span>About us</span>Different kind of Financial firm</h2>
          <p>The mission of the Bizipress Financial Planning Association is to setup, promote and implement high quality
             standards for competence and ethical behavior for the financial advisory sector.</p>
          <div class="gap-15"></div>
          <div class="row">
             <div class="col-lg-6">
                <div class="ts-feature-box">
                   <div class="ts-feature-info">
                      <img src="{{ URL::asset('front-theme/images/pages/color_icon1.png')}}" alt="">
                      <h3 class="ts-feature-title">Best Finance Brand</h3>
                      <p>Mission of Bizipress is Planning Association is to setup</p>
                   </div>
                </div>
                <!-- feature box-1 end-->
             </div>
             <!-- col-1 end-->
             <div class="col-lg-6">
                <div class="ts-feature-box">
                   <div class="ts-feature-info">
                      <img src="{{ URL::asset('front-theme/images/pages/color_icon2.png')}}" alt="">
                      <h3 class="ts-feature-title">ISO Certified</h3>
                      <p>Mission of Bizipress Financial Planning is to promote. </p>
                   </div>
                </div>
                <!-- feature box-2 end-->
             </div>
             <!-- col-2 end-->
          </div>
          <!-- container row end-->
       </div>
       <!-- Col end-->
       <div class="col-lg-6">
          <img class="img-fluid" src="{{ URL::asset('front-theme/images/pages/about_2.png')}}" alt="">
       </div>
       <!-- Col end-->
    </div>
    <!-- Main row end-->
 </div>

 <div class="gap-80"></div>

 <div class="ts-facts-area-bg bg-overlay">
    <div class="container">
       <div class="row facts-wrapper text-center">
          <div class="col-lg-3 col-md-3">
             <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-money-1"></i></span>
                <div class="ts-facts-content">
                   <h4 class="ts-facts-num"><span class="counterUp">2435</span></h4>
                   <p class="facts-desc">Cases Completed</p>
                </div>
             </div>
             <!-- Facts 1 end-->
          </div>
          <!-- Col 1 end-->
          <div class="col-lg-3 col-md-3">
             <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-invest"></i></span>
                <div class="ts-facts-content">
                   <h4 class="ts-facts-num"><span class="counterUp">467</span></h4>
                   <p class="facts-desc">Successful Investment</p>
                </div>
             </div>
             <!-- Facts 2 end-->
          </div>
          <!-- Col 2 end-->
          <div class="col-lg-3 col-md-3">
             <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-chart2"></i></span>
                <div class="ts-facts-content">
                   <h4 class="ts-facts-num"><span class="counterUp">85 </span>%</h4>
                   <p class="facts-desc">Business Growth </p>
                </div>
             </div>
             <!-- Facts 3 end-->
          </div>
          <!-- Col 3 end-->
          <div class="col-lg-3 col-md-3">
             <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-deal"></i></span>
                <div class="ts-facts-content">
                   <h4 class="ts-facts-num"><span class="counterUp">139</span></h4>
                   <p class="facts-desc">Running Projects</p>
                </div>
             </div>
             <!-- Facts 4 end-->
          </div>
          <!-- Col 4 end-->
       </div>
       <!-- Row end-->
    </div>
    <!-- Container 2 end-->
 </div>
 <!-- Facts area end-->
 <div class="gap-80"></div>

 <div class="ts-team">
    <div class="container">
       <div class="row text-center">
          <div class="col-md-12">
             <h2 class="section-title"><span>Our People</span>Best Team</h2>
          </div>
       </div>
       <!-- Title row end-->
       <div class="row">
          <div class="col-lg-3 col-md-6">
             <div class="ts-team-wrapper">
                <div class="team-img-wrapper">
                   <img class="img-fluid" alt="" src="images/team/team1.jpg">
                </div>
                <div class="ts-team-content">
                   <h3 class="team-name">Denise Brewer</h3>
                   <p class="team-designation">Senior Project Manager</p>
                </div>
                <!-- Team content end-->
                <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                   <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                <!-- social-icons-->
             </div>
             <!-- Team wrapper 1 end-->
          </div>
          <!-- Col end-->
          <div class="col-lg-3 col-md-6">
             <div class="ts-team-wrapper">
                <div class="team-img-wrapper">
                   <img class="img-fluid" alt="" src="images/team/team2.jpg">
                </div>
                <div class="ts-team-content">
                   <h3 class="team-name">Patrick Ryan</h3>
                   <p class="team-designation">Senior Project Manager</p>
                </div>
                <!-- Team content end-->
                <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                   <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                <!-- social-icons-->
             </div>
             <!-- Team wrapper 1 end-->
          </div>
          <!-- Col end-->
          <div class="col-lg-3 col-md-6">
             <div class="ts-team-wrapper">
                <div class="team-img-wrapper">
                   <img class="img-fluid" alt="" src="images/team/team3.jpg">
                </div>
                <div class="ts-team-content">
                   <h3 class="team-name">Craig Robinson</h3>
                   <p class="team-designation">Senior Project Manager</p>
                </div>
                <!-- Team content end-->
                <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                   <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                <!-- social-icons-->
             </div>
             <!-- Team wrapper 1 end-->
          </div>
          <!-- Col end-->
          <div class="col-lg-3 col-md-6">
             <div class="ts-team-wrapper">
                <div class="team-img-wrapper">
                   <img class="img-fluid" alt="" src="images/team/team4.jpg">
                </div>
                <div class="ts-team-content">
                   <h3 class="team-name">Andrew Robinson</h3>
                   <p class="team-designation">Senior Project Manager</p>
                </div>
                <!-- Team content end-->
                <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                   <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                <!-- social-icons-->
             </div>
             <!-- Team wrapper 1 end-->
          </div>
          <!-- Col end-->
       </div>
       <!-- Content row end-->
    </div>
    <!-- Container end-->
 </div>
 <!-- Section Team end-->
@endsection
