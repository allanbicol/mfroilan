@extends('front.page-template')

@section('meta-title')
<title>Cost Summary - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<link rel="canonical" href="{{route('costsummary')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta property="og:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta property="og:url" content="{{route('costsummary')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta name="twitter:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Login</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Login</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services" id="ts-services">
        <div class="container">
            
           <div class="card" style="display:block!important;">
                <article class="card-body mx-auto" style="border:none;max-width:500px;">
                    <h4 class="card-title mt-3 text-center">Login</h4>
                    <p>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>{!! $message !!}</div>
                    <?php Session::forget('success');?>
                    @endif
                
                    @if ($message = Session::get('error'))
                    <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>{!! $message !!}</div>
                    <?php Session::forget('error');?>
                    @endif
                    </p>

                    @if(count($errors) > 0)
                    <p>
                        <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </u>

                        </div>
                    </p>
                    @endif

                    <form action="{{route('login-validate')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="email" class="form-control" placeholder="Email address" type="email">
                    </div> <!-- form-group// -->
                    
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="password" class="form-control" placeholder="Enter password" type="password">
                    </div> <!-- form-group// -->
                                                       
                    <div class="form-group">    
                        <button type="submit" class="btn btn-primary btn-block"> Login </button>
                    </div> <!-- form-group// -->      
                    <p class="text-center">Don't have an account? <a href="{{route('register')}}">Sign up</a> </p>                                                                 
                </form>
                </article>
            </div> <!-- card.// -->


        </div>
        <!-- Container end-->
    </div>
    </section>
    @if(Auth::user())
        <script>window.location = "/dashboard";</script>
    @endif
@endsection
