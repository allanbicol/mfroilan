@extends('front.page-template')

@section('meta-title')
<title>Dashboard - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<link rel="canonical" href="{{route('costsummary')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta property="og:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta property="og:url" content="{{route('costsummary')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta name="twitter:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Dashboard</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Dashboard</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services ts-service-pattern" id="ts-services">
        <div class="container">
            <div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Dashboard</a>
      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Purchased Item</a>
      <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Announcement</a>
      <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Settings</a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">Comming Soon</div>
      <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">Comming Soon</div>
      <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">Comming Soon</div>
      <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">Comming Soon</div>
    </div>
  </div>
</div>
        </div>
        <!-- Container end-->
    </div>
    </section>
@endsection
