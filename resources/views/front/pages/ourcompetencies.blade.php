@extends('front.page-template')

@section('meta-title')
<title>Our Competencies - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="We, at MFROILAN TRAINING AND CONSULTANCY believe that we offer competent and efficient service with our stable and experienced writing team. Our writers have written numerous training materials for individualised and institutionalised clients. ">
<link rel="canonical" href="{{route('ourcompetencies')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Our Competencies - MFroilan Training and Consultancy">
<meta property="og:description" content="We, at MFROILAN TRAINING AND CONSULTANCY believe that we offer competent and efficient service with our stable and experienced writing team. Our writers have written numerous training materials for individualised and institutionalised clients. ">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/competencies.jpg')}}">
<meta property="og:url" content="{{route('ourcompetencies')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Our Competencies - MFroilan Training and Consultancy">
<meta name="twitter:description" content="We, at MFROILAN TRAINING AND CONSULTANCY believe that we offer competent and efficient service with our stable and experienced writing team. Our writers have written numerous training materials for individualised and institutionalised clients. ">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/competencies.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Our Competencies</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Our Competencies</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container" id="main-container">
<div class="container">
    <div class="row">
       <div class="col-lg-6">
          <h2 class="column-title title-small"><span>Our Competencies</span></h2>
          <p>We, at <b>MFROILAN TRAINING AND CONSULTANCY</b> believe that we offer competent and efficient service
                with our stable and experienced writing team. Our writers have written numerous training materials
                for individualised and institutionalised clients. We can:</p>
          <div class="gap-15"></div>
          <div class="row">
             <div class="col-lg-12">
                <div class="ts-feature-box">
                   <div class="ts-feature-info">
                      <ul>
                          <li class="list">Provide organised training materials in different areas of study</li>
                          <li class="list">Make training materials focused on the competency requirement sought to be achieved</li>
                           <li class="list">Follow training material format in accordance to the organisational requirements of the
                                institution or company; and</li>
                           <li class="list">Offer consultancy as to the contents required by the client</li>
                      </ul>

                   </div>
                </div>
                <!-- feature box-1 end-->
             </div>

          </div>
          <!-- container row end-->
       </div>
       <!-- Col end-->
       <div class="col-lg-6">
          <img class="img-fluid" src="{{ URL::asset('front-theme/images/pages/about_2.png')}}" alt="">
       </div>
       <!-- Col end-->
    </div>
    <!-- Main row end-->
 </div>

 <div class="gap-80"></div>

</section>


@endsection
