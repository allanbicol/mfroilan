@extends('front.page-template')

@section('meta-title')
<title>Cost Summary - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<link rel="canonical" href="{{route('costsummary')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta property="og:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta property="og:url" content="{{route('costsummary')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Cost Summary - MFroilan Training and Consultancy">
<meta name="twitter:description" content="We want you to control your operating cost but at the same time comply with industry standards.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/costsummary.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Cost Summary</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Cost Summary</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services ts-service-pattern" id="ts-services">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                <h2 class="section-title">Cost Summary</h2>
                {{-- <p>We specifically provide the following training materials to our clients.</p> --}}
                </div>
            </div>
            <!-- Title row end-->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <p>Schedule of fees are as follows on a per unit basis:</p>
                        <br>
                    <table class="table table-striped">

                        <tbody>
                            <tr>
                            <td>Learner guide and PowerPoint presentation</td>
                            <td>$400</td>
                            </tr>
                            <tr>
                            <td>Assessment Tools per unit (Assessments, Assessor Guide, Mapping Tool)</td>
                            <td>$250</td>
                            </tr>
                            <tr>
                            <td>Mapping Tool (if done separately)</td>
                            <td>$75</td>
                            </tr>
                            <td>Mapping</td>
                            <td>$20</td>
                            </tr>
                            <td>Training Assessment Strategy (TAS)</td>
                            <td>$50</td>
                            </tr>
                            <td>Re-branding (transfer of documents to RTO’s new templates)</td>
                            <td>$300</td>
                            </tr>

                        </tbody>
                    </table>
                    <br><br>
                    <p>If you wish to pay us on a per hour basis, we offer the following package:</p>
                    <p><b>$800 per week -</b>This way you dictate how many hours should your allocated tasks be completed. This amount covers all administrative tasks you want us to complete for you whether it is reformatting your files or rebranding your materials.</p>
                    <p>We want you to control your operating cost but at the same time comply with industry standards.</p>
                    {{-- <p>All training materials will include Learner Guide in MS Word format and Trainer’s PowerPoint
                            Presentation Material. We do not limit revisions because we want our clients to get what they pay
                            for.</p> --}}

                    {{-- <div class="row" >
                        <div class="col-lg-6" style="background-color:#e3e3e3;border-radius:10px;padding:20px;margin-left:15px;">
                            <div class="row">
                                <div class="col-lg-12" style="color:#0f2765;">Terms of payment are as follows:</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-10">
                                    Commitment Fee- Upon Award of the Project
                                </div>
                                <div class="col-lg-2">
                                    30%
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-10">
                                    Submission of Final Version/Completion
                                </div>
                                <div class="col-lg-2">
                                    70%
                                </div>
                            </div>
                        </div>
                    </div> --}}

                </div>

            </div>

        </div>
        <!-- Container end-->
    </div>
    </section>
@endsection
