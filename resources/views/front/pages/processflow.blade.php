@extends('front.page-template')

@section('meta-title')
<title>Process Flow - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="MFROILAN TRAINING AND CONSULTANCY Process Flow">
<link rel="canonical" href="{{route('processflow')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Process Flow - MFroilan Training and Consultancy">
<meta property="og:description" content="MFROILAN TRAINING AND CONSULTANCY Process Flow">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/processflow.jpg')}}">
<meta property="og:url" content="{{route('processflow')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Process Flow  - MFroilan Training and Consultancy">
<meta name="twitter:description" content="MFROILAN TRAINING AND CONSULTANCY Process Flow">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/processflow.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Process Flow</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Process Flow</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services ts-service-pattern" id="ts-services">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                <h2 class="section-title">Process Flow</h2>
                {{-- <p>We specifically provide the following training materials to our clients.</p> --}}
                </div>
            </div>
            <!-- Title row end-->
            <div class="row">
                <div class="col-lg-12 col-md-12">

                        <div class="row">
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">ENQUIRY</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Negotiation and submission of sample <br>materials and formats to be followed</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Acceptance of project terms</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Payment of 30%  Commitment Fee</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Commencement</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Submission of first draft <br>(timeframe is 3 to 5 days per subject matter)</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Review by the client</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Revision (timeframe is 1 day)</p></div>
                                <div class="col-lg-12 text-center"><span class="fa fa-arrow-down"></span></div>
                                <div class="col-lg-12 text-center"><p class="btn btn-lg bg-blue" style="white-space: normal;">Submission of final version</p></div>
                        </div>

                </div>

            </div>

        </div>
        <!-- Container end-->
    </div>
    </section>
@endsection
