@extends('front.page-template')

@section('meta-title')
<title>Blog - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective. ">
<link rel="canonical" href="{{route('whoweare')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Blog - MFroilan Training and Consultancy">
<meta property="og:description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective. ">
<meta property="og:image" content="{{ URL::asset('front-theme/images/pages/about_1.png')}}">
<meta property="og:url" content="{{route('whoweare')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Blog - MFroilan Training and Consultancy">
<meta name="twitter:description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective. ">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/pages/about_1.png')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Blog</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Blog</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container" id="main-container">
<div class="container">
    <div class="row">
        <div class="col-lg-4">
           <div class="sidebar sidebar-left">
              <div class="widget widget-search">
                 <div class="input-group" id="search">
                    <input class="form-control" placeholder="Search" type="search"><span class="input-group-btn"><i class="fa fa-search"></i></span>
                 </div>
              </div>
              <div class="widget recent-posts">
                 <h3 class="widget-title">Popular Posts</h3>
                 <ul class="unstyled clearfix">
                    <li class="media">
                        <div class="media-left media-middle">
                            <img alt="img" src="{{ URL::asset('front-theme/images/blog/blog2.jpg')}}">
                        </div>
                        <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 13 Nov, 2019</a></span>
                            <h4 class="entry-title"><a href="{{route('blog2')}}">5 RTO Functions That Can Be Completed Offshore</a>
                                <small>by Mith</small>
                            </h4>
                        </div>
                        <div class="clearfix"></div>
                        </li>
                    <!-- 1st post end-->
                    <li class="media">
                        <div class="media-left media-middle">
                            <img alt="img" src="{{ URL::asset('front-theme/images/blog/blog1.jpg')}}">
                        </div>
                        <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 12 Nov, 2019</a></span>
                            <h4 class="entry-title"><a href="{{route('blog1')}}">Two Tips to Sustain Your RTO’s Financial Life</a>
                                <small>by Mith</small>
                            </h4>
                        </div>
                        <div class="clearfix"></div>
                        </li>
                    <!-- 2nd post end-->

                 </ul>
              </div>
              <!-- Recent post end-->
              {{-- <div class="widget">
                 <h3 class="widget-title">Categories</h3>
                 <ul class="widget-nav-tabs">
                    <li><a href="#">Tax Planning</a><span class="posts-count">(05)</span></li>
                    <li><a href="#">Saving Strategy</a><span class="posts-count">(06)</span></li>
                    <li><a href="#">Financial Planning</a><span class="posts-count">(11)</span></li>
                    <li><a href="#">Mutual Funds</a><span class="posts-count">(09)</span></li>
                    <li><a href="#">Business Loan</a><span class="posts-count">(13)</span></li>
                    <li><a href="#">Insurance Consulting</a><span class="posts-count">(13)</span></li>
                 </ul>
              </div> --}}
              <!-- Categories end-->
              {{-- <div class="widget">
                 <h3 class="widget-title">Archives </h3>
                 <ul class="widget-nav-tabs">
                    <li><a href="#">Feburay 2016</a></li>
                    <li><a href="#">January 2016</a></li>
                    <li><a href="#">December 2015</a></li>
                    <li><a href="#">November 2015</a></li>
                    <li><a href="#">October 2015</a></li>
                 </ul>
              </div> --}}
              <!-- Archives end-->
              {{-- <div class="widget widget-ad">
                 <div class="widget-ad-bg bg-overlay overlay-color">
                    <p>Looking for Business support</p><a class="btn btn-primary" href="#">Read More</a>
                 </div>
              </div>
              <div class="widget widget-tags">
                 <h3 class="widget-title">Tags </h3>
                 <ul class="unstyled clearfix">
                    <li><a href="#">Construction</a></li>
                    <li><a href="#">Design</a></li>
                    <li><a href="#">Project</a></li>
                    <li><a href="#">Building</a></li>
                    <li><a href="#">Finance</a></li>
                    <li><a href="#">Safety</a></li>
                    <li><a href="#">Contracting</a></li>
                    <li><a href="#">Planning</a></li>
                 </ul>
              </div> --}}
              <!-- Tags end-->
           </div>
           <!-- Sidebar end-->
        </div>
        <!-- Sidebar Col end-->
        <div class="col-lg-8">
                <div class="post">
                        <div class="post-media post-image">
                           <img class="img-fluid" src="{{ URL::asset('front-theme/images/blog/blog2.jpg')}}" alt="">
                        </div>
                        <div class="post-body clearfix">
                           <div class="post-meta-left pull-left text-center"><span class="post-meta-date"><span class="day">13</span>Nov</span><span class="post-author"><img class="avatar" alt="" src="images/news/avator1.png"><a href="#"> By Mith</a></span>
                              <span class="post-comment"><i class="icon icon-comment"></i><a class="comments-link" href="#"></a>0</span>
                           </div>
                           <!-- Post meta left-->
                           <div class="post-content-right">
                              <div class="entry-header">
                                 {{-- <div class="post-meta"><span class="post-cat"><i class="icon icon-folder"></i><a href="#"> Financial Planning</a></span>
                                    <span class="post-tag"><i class="icon icon-tag"></i><a href="#"> Insurance, Business</a></span>
                                 </div> --}}
                                 <h2 class="entry-title"><a href="{{route('blog2')}}">5 RTO Functions That Can Be Completed Offshore</a></h2>
                              </div>
                              <!-- header end-->
                              <div class="entry-content">
                                 <p>Technology has changed how RTO functions can be done. In the past, hiring local employees is the only solution to handle business concerns. When you need something encoded, you need to hire a typist and when you need someone to take in calls, you need to hire an administrative assistant. These RTO functions have evolved. These days, you may resort to other options when you need some RTO functions completed the soonest. Aside from using business solutions like online applications or software, RTO functions can now be done offshore.
                                 </p>
                              </div>
                              <div class="post-footer text-right"><a class="btn btn-primary" href="{{route('blog2')}}">Continue Reading</a></div>
                           </div>
                           <!-- Post content right-->
                        </div>
                        <!-- post-body end-->
                     </div>
                       <!-- 2nd post end-->

           <div class="post">
              <div class="post-media post-image">
                 <img class="img-fluid" src="{{ URL::asset('front-theme/images/blog/blog1.jpg')}}" alt="">
              </div>
              <div class="post-body clearfix">
                 <div class="post-meta-left pull-left text-center"><span class="post-meta-date"><span class="day">12</span>Nov</span><span class="post-author"><img class="avatar" alt="" src="images/news/avator1.png"><a href="#"> By Mith</a></span>
                    <span class="post-comment"><i class="icon icon-comment"></i><a class="comments-link" href="#"></a>0</span>
                 </div>
                 <!-- Post meta left-->
                 <div class="post-content-right">
                    <div class="entry-header">
                       {{-- <div class="post-meta"><span class="post-cat"><i class="icon icon-folder"></i><a href="#"> Financial Planning</a></span>
                          <span class="post-tag"><i class="icon icon-tag"></i><a href="#"> Insurance, Business</a></span>
                       </div> --}}
                       <h2 class="entry-title"><a href="{{route('blog1')}}">Two Tips to Sustain Your RTO’s Financial Life</a></h2>
                    </div>
                    <!-- header end-->
                    <div class="entry-content">
                       <p>I’ve been working in the VET sector for more than 8 years now. I’ve read and heard RTOs shutting down operations due to unmet government and private obligations after 10 or 5 years of operations. This is too sad to say the least as gigantic efforts have been put in place to kickstart RTO operations. Thus, here are two tips you can observe to sustain your RTO’s financial life and ensure that annual turnover is way higher than your budget.
                       </p>
                    </div>
                    <div class="post-footer text-right"><a class="btn btn-primary" href="{{route('blog1')}}">Continue Reading</a></div>
                 </div>
                 <!-- Post content right-->
              </div>
              <!-- post-body end-->
           </div>
           <!-- 1st post end-->


        </div>
        <!-- Content Col end-->
     </div>
     <!-- Main row end-->
 </div>

 <div class="gap-80"></div>

</section>


@endsection
