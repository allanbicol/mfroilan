@extends('front.page-template')
@section('meta-title')
<title>What We Do - Mfroilan Training and Consultancy</title>
@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Completed Projects</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Projects</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services ts-service-pattern" id="ts-services">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                <h2 class="section-title">Completed Projects</h2>
                </div>
            </div>
            <!-- Title row end-->
            <div class="row">
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="{{ URL::asset('front-theme/images/services/service1.jpg')}}" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            {{-- <i class="icon icon-consult"></i> --}}
                        </span>
                        <div style="font-size:14px">Assessment tools for Marketing, Business and OHS units</div>
                        <h3 class="service-title">NUA Horizons</h3>
                        <p>Brisbane, Australia</p>

                    </div>
                </div>
                <!-- Service1 end-->
                </div>
                <!-- Col 1 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service2.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            {{-- <i class="icon icon-consult"></i> --}}
                        </span>
                        <div style="font-size:14px">Assessment tools for Holistic Medicine units</div>
                        <h3 class="service-title">Australian Institution of Holistic Medicine</h3>
                        <p>Australia</p>
                    </div>
                </div>
                <!-- Service2 end-->
                </div>
                <!-- Col 2 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service3.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <img src="{{ URL::asset('front-theme/images/clients/client1.png')}}" width="150px;"/>
                        </span>
                        <div style="font-size:14px">Assessment tools for OHS units</div>
                        <h3 class="service-title">WH&S Training and Assessment Services</h3>
                        <p>Australia</p>
                    </div>
                </div>
                <!-- Service3 end-->
                </div>
                <!-- Col 3 end-->
            </div>
            <!-- Content 1 row end-->
            <div class="gap-60"></div>
            <div class="row">
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service4.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <img src="{{ URL::asset('front-theme/images/clients/client2.png')}}" style="margin-top:20px;" width="150px;"/>
                        </span>
                        <div style="font-size:14px">Training Materials on Business and Information Technology</div>
                        <h3 class="service-title">Knowledge Plus</h3>
                        <p>Australia</p>
                    </div>
                </div>
                <!-- Service4 end-->
                </div>
                <!-- Col 4 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service5.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <img src="{{ URL::asset('front-theme/images/clients/client4.png')}}" style="margin-top:10px;" width="170px;"/>
                        </span>
                        <div style="font-size:14px">Training Materials on the Seafood Industry</div>
                        <h3 class="service-title">Seafood Training Services</h3>
                        <p>New Zealand</p>
                    </div>
                </div>
                <!-- Service5 end-->
                </div>
                <!-- Col 5 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service6.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            {{-- <i class="icon icon-consult"></i> --}}
                        </span>
                        <div style="font-size:14px">Training Materials on Business-related Topics</div>
                        <h3 class="service-title">Mind Design Professional</h3>
                        <p>Belgium</p>
                    </div>
                </div>
                <!-- Service6 end-->
                </div>
                <!-- Col 6 end-->
            </div>

            <div class="gap-60"></div>
            <div class="row">
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service6.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            {{-- <i class="icon icon-consult"></i> --}}
                        </span>
                        <div style="font-size:14px">Training Materials on Personality Development</div>
                        <h3 class="service-title">MindLife Success </h3>
                        <p>Singapore</p>
                    </div>
                </div>
                <!-- Service7 end-->
                </div>
                <!-- Col 7 end-->

                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service6.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <img src="{{ URL::asset('front-theme/images/clients/client5.svg')}}" style="margin-top:20px;" width="150px;"/>
                        </span>
                        <div style="font-size:14px">Training Materials on Community Services</div>
                        <h3 class="service-title">Charlton Brown</h3>
                        <p>Brisbane, Australia</p>
                    </div>
                </div>
                <!-- Service8 end-->
                </div>
                <!-- Col 8 end-->

                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service6.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <img src="{{ URL::asset('front-theme/images/clients/client7.png')}}" style="margin-top:10px;" width="120px;"/>
                        </span>
                        <div style="font-size:14px">Training Materials on Fitness</div>
                        <h3 class="service-title">Fitness Industry of Training</h3>
                        <p>Brisbane, Australia</p>
                    </div>
                </div>
                <!-- Service9 end-->
                </div>
                <!-- Col 9 end-->

            </div>

            <div class="gap-60"></div>
            <div class="row">
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service6.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <img src="{{ URL::asset('front-theme/images/clients/client6.png')}}" style="margin-top:20px;" width="150px;"/>
                        </span>
                        <div style="font-size:14px">Training Materials on Hospitality</div>
                        <h3 class="service-title">Training Institute of Australasia</h3>
                        <p>South Perth, West Australia</p>
                    </div>
                </div>
                <!-- Service9 end-->
                </div>
                <!-- Col 9 end-->

                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service6.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <img src="{{ URL::asset('front-theme/images/clients/client3.png')}}" style="margin-top:20px;" width="170px;"/>
                        </span>
                        <div style="font-size:14px">Training Materials on Business and Marketing</div>
                        <h3 class="service-title">Food for Life</h3>
                        <p>Australia</p>
                    </div>
                </div>
                <!-- Service9 end-->
                </div>
                <!-- Col 9 end-->


            </div>
            <!-- Content Row 2 end-->
        </div>
        <!-- Container end-->
    </div>

    </section>
@endsection
