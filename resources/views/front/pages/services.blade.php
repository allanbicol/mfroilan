@extends('front.page-template')

@section('meta-title')
<title>What We Do - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Competency-based trainings, Trainer’s Manual, Session Plan, Trainer’s PowerPoint Presentation, Student Handbook, Assessment Tools, Mapping Tools, Ebooks, Client website articles related to training and education">
<link rel="canonical" href="{{route('whatwedo')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="What We Do - MFroilan Training and Consultancy">
<meta property="og:description" content="Competency-based trainings, Trainer’s Manual, Session Plan, Trainer’s PowerPoint Presentation, Student Handbook, Assessment Tools, Mapping Tools, Ebooks, Client website articles related to training and education">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/whatwedo.jpg')}}">
<meta property="og:url" content="{{route('whatwedo')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="What We Do  - MFroilan Training and Consultancy">
<meta name="twitter:description" content="Competency-based trainings, Trainer’s Manual, Session Plan, Trainer’s PowerPoint Presentation, Student Handbook, Assessment Tools, Mapping Tools, Ebooks, Client website articles related to training and educationw">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/whatwedo.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">What We Do</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>What We Do</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services ts-service-pattern" id="ts-services">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                <h2 class="section-title"><span>Our Services</span>What We Do</h2>
                <p>Mfroilan Training and Consultancy is here to help you meet your organisational requirements.<br>Send us an email or give us a call and we’d be there to be of service to you.</p>
                </div>
            </div>
            <br>
            <br>
            <!-- Title row end-->
            <div class="row">
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="{{ URL::asset('front-theme/images/services/service1.jpg')}}" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <i class="icon icon-consult"></i>
                        </span>
                        <h3 class="service-title">Resource Development</h3>
                        <p>Do you need to purchase or update your training resources? </p>
                        {{-- <p><a class="link-more" href="#">Read More <i class="icon icon-right-arrow2"></i></a></p> --}}
                    </div>
                </div>
                <!-- Service1 end-->
                </div>
                <!-- Col 1 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service2.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <i class="icon icon-consult"></i>
                        </span>
                        <h3 class="service-title">Compliance Management</h3>
                        <p>Do need to conduct an internal audit of your processes and documentation to ensure they are compliant?</p>
                        {{-- <p><a class="link-more" href="#">Read More <i class="icon icon-right-arrow2"></i></a></p> --}}
                    </div>
                </div>
                <!-- Service2 end-->
                </div>
                <!-- Col 2 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service3.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <i class="icon icon-consult"></i>
                        </span>
                        <h3 class="service-title">Professional Development Monitoring</h3>
                        <p>Do you want to ensure your staff complete their professional development trainings?</p>
                        {{-- <p><a class="link-more" href="#">Read More <i class="icon icon-right-arrow2"></i></a></p> --}}
                    </div>
                </div>
                <!-- Service3 end-->
                </div>
                <!-- Col 3 end-->
            </div>
            <!-- Content 1 row end-->
            <div class="gap-60"></div>
            <div class="row">
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service4.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <i class="icon icon-consult"></i>
                        </span>
                        <h3 class="service-title">Student support</h3>
                        <p>Do you need someone to check in with your students on a regular basis and monitor their course progress?</p>
                        {{-- <p><a class="link-more" href="#">Read More <i class="icon icon-right-arrow2"></i></a></p> --}}
                    </div>
                </div>
                <!-- Service4 end-->
                </div>
                <!-- Col 4 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service5.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <i class="icon icon-consult"></i>
                        </span>
                        <h3 class="service-title">LMS Management and Maintenance</h3>
                        <p>Have you got no time to look at whether your learnning management system need an update or upgrade?</p>
                        {{-- <p><a class="link-more" href="#">Read More <i class="icon icon-right-arrow2"></i></a></p> --}}
                    </div>
                </div>
                <!-- Service5 end-->
                </div>
                <!-- Col 5 end-->
                <div class="col-lg-4 col-md-12">
                <div class="ts-service-box">
                    <div class="ts-service-image-wrapper">
                        {{-- <img class="img-fluid" src="images/services/service6.jpg" alt=""> --}}
                    </div>
                    <div class="ts-service-content">
                        <span class="ts-service-icon">
                            <i class="icon icon-consult"></i>
                        </span>
                        <h3 class="service-title">Annual Corporate Plannning</h3>
                        <p>Do you need to bounce and redirect your business to ensure you get your desired results?</p>
                        {{-- <p><a class="link-more" href="#">Read More <i class="icon icon-right-arrow2"></i></a></p> --}}
                    </div>
                </div>
                <!-- Service6 end-->
                </div>
                <!-- Col 6 end-->
            </div>

            
            <!-- Content Row 2 end-->
        </div>
        <!-- Container end-->
    </div>
    </section>
@endsection
