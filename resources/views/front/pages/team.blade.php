@extends('front.page-template')

@section('meta-title')
<title>Our Team - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Mith Froilan, Greta Glory C. Bastalino, Pauline Beatrice Balitaan">
<link rel="canonical" href="{{route('team')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Our Team - MFroilan Training and Consultancy">
<meta property="og:description" content="Mith Froilan, Greta Glory C. Bastalino, Pauline Beatrice Balitaan">
<meta property="og:image" content="{{ URL::asset('front-theme/images/team/team1.jpg')}}">
<meta property="og:url" content="{{route('team')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Our Team  - MFroilan Training and Consultancy">
<meta name="twitter:description" content="Mith Froilan, Greta Glory C. Bastalino, Pauline Beatrice Balitaan">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/team/team1.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Our Team</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Our Team</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container" id="main-container">

        <div class="container">
           <div class="row text-center">
              <div class="col-lg-12">
                 <h2 class="section-title"><span>Our People</span>Best Team</h2>
              </div>
           </div>
           <!-- Title row end-->
           <div class="row">
              <div class="col-lg-4">
                 <div class="ts-team-classic">
                    <div class="team-img-wrapper">
                       <img class="img-responsive" alt="" src="{{ URL::asset('front-theme/images/team/team1.jpg')}}">
                    </div>
                    <div class="ts-team-info">
                       <h3 class="team-name">Mith Froilan</h3>
                       <p class="team-designation">Director</p>
                       <p>
                           Juris Doctor
                           <br><small>University of the East</small>
                           <br><br>Bachelor in Business Administration
                           <br><small>University of the Philippines</small>
                        </p>
                        <p>She carries with her more than 8 years of work experience in the Vocational, Education and Training industry in Australia.</p>
                       <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                          <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                       <!-- social-icons-->
                    </div>
                    <!-- Team info 1 end-->
                 </div>
                 <!-- Team classic 1 end-->
              </div>
              <!-- Col end-->
              <div class="col-lg-4">
                 <div class="ts-team-classic">
                    <div class="team-img-wrapper">
                       <img class="img-responsive" alt="" src="{{ URL::asset('front-theme/images/team/team2.jpg')}}">
                    </div>
                    <div class="ts-team-info">
                       <h3 class="team-name">Greta Glory C. Bastalino</h3>
                       <p class="team-designation">Trainer/Writer </p>
                       <p>Bachelor of Laws <br> <small>University of Eastern Philippines</small></p>
                       <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                          <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                       <!-- social-icons-->
                    </div>
                    <!-- Team info 2 end-->
                 </div>
                 <!-- Team classic 2 end-->
              </div>
              <!-- Col end-->
              <div class="col-lg-4">
                 <div class="ts-team-classic">
                    <div class="team-img-wrapper">
                       <img class="img-responsive" alt="" src="{{ URL::asset('front-theme/images/team/team3.jpg')}}">
                    </div>
                    <div class="ts-team-info">
                       <h3 class="team-name">Pauline Beatrice Balitaan</h3>
                       <p class="team-designation">Trainer/Writer</p>
                       <p>
                           Master’s Degree in Family Life and Child Development
                        <br><small>University of the Philippines</small>
                       </p>
                       <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                          <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                       <!-- social-icons-->
                    </div>
                    <!-- Team info 3 end-->
                 </div>
                 <!-- Team classic 3 end-->
              </div>
              <!-- Col end-->
           </div>
           <!-- Content row end-->
        </div>
     </section>
@endsection
