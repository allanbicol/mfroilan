@extends('front.page-template')

@section('meta-title')
<title>Benefits we offer - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="MFROILAN TRAINING AND CONSULTANCY does not only provide clients with content-oriented training materials, rather we also provide customised training materials tailor-fitted to the needs of the trainees. For every training material we create, we offer direct access with our clients through Email or Skype. We will afford you the most amount of feedback possible to make sure we created a winning training material.">
<link rel="canonical" href="{{route('benefitsweoffer')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Benefits we offer - MFroilan Training and Consultancy">
<meta property="og:description" content="MFROILAN TRAINING AND CONSULTANCY does not only provide clients with content-oriented training materials, rather we also provide customised training materials tailor-fitted to the needs of the trainees. For every training material we create, we offer direct access with our clients through Email or Skype. We will afford you the most amount of feedback possible to make sure we created a winning training material.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/socialimages/benefits.jpg')}}">
<meta property="og:url" content="{{route('benefitsweoffer')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Benefits we offer - MFroilan Training and Consultancy">
<meta name="twitter:description" content="MFROILAN TRAINING AND CONSULTANCY does not only provide clients with content-oriented training materials, rather we also provide customised training materials tailor-fitted to the needs of the trainees. For every training material we create, we offer direct access with our clients through Email or Skype. We will afford you the most amount of feedback possible to make sure we created a winning training material.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/socialimages/benefits.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Benefits We Offer</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Benefits We Offer</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container no-padding" id="main-container">
    <div class="ts-services ts-service-pattern" id="ts-services">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                <h2 class="section-title">Benefits We Offer</h2>
                {{-- <p>We specifically provide the following training materials to our clients.</p> --}}
                </div>
            </div>
            <!-- Title row end-->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <p>
                            <b>MFROILAN TRAINING AND CONSULTANCY</b> does not only provide clients with content-oriented training
                            materials, rather we also provide customised training materials tailor-fitted to the needs
                            of the trainees. For every training material we create, we offer direct access with our
                            clients through Email or Skype. We will afford you the most amount of feedback possible
                            to make sure we created a winning training material.
                    </p>
                    <p>
                            We do not regard other companies offering the same services as a competitor; rather we look
                            at them as partners to whom we can also lend our services at a reasonable service cost.
                            We can estimate our service cost within a fair price from which partner companies can also
                            take a reasonable cut to add to their earnings. Our doors are open to negotiate on this matter.
                    </p>
                </div>

            </div>

        </div>
        <!-- Container end-->
    </div>
    </section>
@endsection
