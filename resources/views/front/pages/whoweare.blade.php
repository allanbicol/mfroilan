@extends('front.page-template')

@section('meta-title')
<title>Who We Are - Mfroilan Training and Consultancy</title>
@endsection

@section('meta')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective. ">
<link rel="canonical" href="{{route('whoweare')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Who We Are - MFroilan Training and Consultancy">
<meta property="og:description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective. ">
<meta property="og:image" content="{{ URL::asset('front-theme/images/pages/about_1.png')}}">
<meta property="og:url" content="{{route('whoweare')}}">
<meta property="og:site_name" content="MFroilan Training and Consultancy">

<meta name="twitter:title" content="Who We Are  - MFroilan Training and Consultancy">
<meta name="twitter:description" content="MFOILAN TRAINING AND CONSULTANCY is a company specialising on developing training materials for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United States and rendering competency-based trainings to domestic institutions. Delivering well- researched training materials on time is our primary objective. ">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/pages/about_1.png')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('title')
    <div class="banner-heading">
        <h1 class="banner-title">Who We Are</h1>
        <ol class="breadcrumb">
        <li><a href="{{route('homepage')}}">Home</a></li>
        <li>Who We Are</li>
        </ol>
    </div>
@endsection

@section('content')
<section class="main-container" id="main-container">
<div class="container">
    <div class="row">
       <div class="col-lg-6">
          <h2 class="column-title title-small"><span>Who we are</span></h2>
          <p><b>MFOILAN TRAINING AND CONSULTANCY</b> is a company specialising on developing training materials
            for the Vocational, Education, and Training sector in Australia, New Zealand, Singapore and United
            States and rendering competency-based trainings to domestic institutions. Delivering well-
            researched training materials on time is our primary objective. We also aim to:</p>
          <div class="gap-15"></div>
          <div class="row">
             <div class="col-lg-12">
                <div class="ts-feature-box">
                   <div class="ts-feature-info">
                      <ul>
                          <li class="list">Provide our clients with training materials free from grammatical errors and plagiarism;</li>
                          <li class="list">Accomplish projects in accordance to the job requirements established by the clients and by
                            the government of Australia on trainings;</li>
                           <li class="list">Provide consultancy on training materials and related dealings; and</li>
                           <li class="list">Hire, develop and train more staff for the team in the future</li>
                      </ul>

                   </div>
                </div>
                <!-- feature box-1 end-->
             </div>

          </div>
          <!-- container row end-->
       </div>
       <!-- Col end-->
       <div class="col-lg-6">
          <img class="img-fluid" src="{{ URL::asset('front-theme/images/pages/about_2.png')}}" alt="">
       </div>
       <!-- Col end-->
    </div>
    <!-- Main row end-->
 </div>

 <div class="gap-80"></div>

</section>


@endsection
