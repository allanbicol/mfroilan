<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    protected $fillable = ['name','client_name','website','description'];

    public $timestamps = false;
}
