<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLevelTwo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_level_two';



    public $timestamps = false;
}
