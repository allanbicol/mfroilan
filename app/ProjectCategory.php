<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_category';

    protected $fillable = ['name','slug','created_at'];
    protected $guarded = ['description'];

    public $timestamps = false;
}
