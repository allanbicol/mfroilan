<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Hash;

class FrontAuthController extends Controller
{
    function loginPage(){
        return view('front.pages.account.login');
    }

    function registerPage(){
        return view('front.pages.account.register');
    }

    public function signup(Request $request)
    {
        $request->validate([
            'fullname' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation'=>'sometimes|required_with:password',
        ]);
        $user = new User([
            'name' => $request->fullname,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $user->save();

        return redirect()->route('login')->with('success','You are successfully registered. You can now login and manage your account.');
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)){
            return back()->with('error','Wrong Login Details');
        }
            // return response()->json([
            //     'message' => 'Unauthorized'
            // ], 401);
        // $user = $request->user();
        // $tokenResult = $user->createToken('Personal Access Token');
        // $token = $tokenResult->token;
        
        // if ($request->remember_me)
        //     $token->expires_at = Carbon::now()->addWeeks(1);
        // $token->save();
        // return response()->json([
        //     'access_token' => $tokenResult->accessToken,
        //     'token_type' => 'Bearer',
        //     'expires_at' => Carbon::parse(
        //         $tokenResult->token->expires_at
        //     )->toDateTimeString()
        // ]);
        return redirect()->route('account-dashboard');
    }

    function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
}
