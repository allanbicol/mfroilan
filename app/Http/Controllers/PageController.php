<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use Mail;

class PageController extends Controller
{
    function index()
    {
        return view('front.pages.index');
    }

    function aboutus()
    {
        return view('front.pages.aboutus');
    }

    function whoweare()
    {
        return view('front.pages.whoweare');
    }

    function ourcompetencies()
    {
        return view('front.pages.ourcompetencies');
    }

    function costsummary()
    {
        return view('front.pages.costsummary');
    }

    function processflow()
    {
        return view('front.pages.processflow');
    }

    function benefitsweoffer()
    {
        return view('front.pages.benefits');
    }

    function portfolio()
    {
        return view('front.pages.portfolio');
    }

    function services()
    {
        return view('front.pages.services');
    }

    function team()
    {
        return view('front.pages.team');
    }


    function contact()
    {
        return view('front.pages.contact');
    }

    function blog()
    {
        return view('front.pages.blog');
    }
    function blog1()
    {
        return view('front.pages.blog.blog1');
    }
    function blog2()
    {
        return view('front.pages.blog.blog2');
    }

    function newproject(){
        return view('front.pages.new-project');
    }

    function sendEmail(Request $request)
    {

        $data = [
            'name'=>$request->input('name'),
            'company'=>$request->input('company'),
            'email'=>$request->input('email'),
            'phone'=>$request->input('phone'),
            'service'=>$request->input('service'),
            'message'=>$request->input('message'),
        ];
        // dump($data);
        // print_r($data['services']);
        // return view('front.mail.contact',['data'=>$data]);
        // exit();

       // Mail::to('bicolallan@gmail.com')->send(new SendMail($data));

        $email = Mail::send('front.mail.contact', ['data'=>$data], function($message) {
            $message->to('mith@mfroilan.com');
            $message->from('no-reply@mfroilan.com',"Mfroilan Training and Consultancy");
            $message->subject('Mfroilan inquiry');
        });

        return back()->with('success','Message sent successfully!');
    }
}
