<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CategoryLevelOne;
use App\CategoryLevelTwo;
use App\CategoryLevelThree;
use App\MyHomeData;

class CategoryController extends Controller
{
    public function getCategoryOne()
    {
        return CategoryLevelOne::all();
    }


    public function getCategoryTwo($parent_id)
    {
        $category = CategoryLevelTwo::where('level_one_id', $parent_id)->get();
        return response()->json($category);
    }

    public function getCategoryThree($parent_id)
    {
        $category = CategoryLevelThree::where('level_two_id', $parent_id)->get();
        return response()->json($category);
    }

    public function savePoolSearch(Request $request){
        $this->validate($request,[
            'username'     =>  'required',
            'name'  =>  'required'
        ]);

        $project =  new MyHomeData();

        $project->customer_id    =   $request->username;
        $project->name           =   $request->name;
        $project->url            =   $request->url;
        $project->original_name  =   $request->name;
        $project->brand          =   $request->brand;
        $project->save();

        return response()->json(['success' => 'success'], 200);

    }

    public function getPoolSearch(Request $request)
    {
        $data = MyHomeData::where('customer_id', $request->username)->get();
        return response()->json($data);
    }
}
