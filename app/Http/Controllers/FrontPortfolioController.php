<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontPortfolioController extends Controller
{
    function project1()
    {
        return view('front.pages.portfolio.project1');
    }

    function project2()
    {
        return view('front.pages.portfolio.project2');
    }
}
