<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLevelThree extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_level_three';



    public $timestamps = false;
}
