<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLevelOne extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_level_one';



    public $timestamps = false;
}
