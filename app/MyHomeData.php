<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyHomeData extends Model
{
      /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'my_home_data';



    public $timestamps = false;
}
